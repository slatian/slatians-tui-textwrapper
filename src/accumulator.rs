
/// An accumulator to hold some values across multiple wrapping functions.
/// 
/// Actual contents and their meaning are not guarantted to be stable.
#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Accumulator {
	/// Text width since real line start
	pub width: usize,

	/// Number of characters since real line start
	pub offset: usize,

	/// The real line length to aim for when in balanced wrap mode.
	pub aim_for_line_length: usize,
}

impl Accumulator {

	/// Creates a new, blank accumulator with all values initalized to zero.
	pub fn new() -> Self {
		Self {
			width: 0,
			offset: 0,
			aim_for_line_length: 0,
		}
	}

	// Causes the accumulator to account for the next real line.
	// Resets valus relative to the "current" real line.
	pub fn next_real_line(&mut self) {
		self.reset_real_line();
	}

	// Resets valus relative to the "current" real line.
	// Sutable for retrying a piece of text with a different algorithm.
	pub fn reset_real_line(&mut self) {
		self.width = 0;
		self.offset = 0;
	}
}

