use slatians_tui_textwrapper::*;

pub fn main() {
	let wrapped_text = wrap_text(
		"The quick brown fox🦊 jumps over the lazy dog🐕.",
		&Settings::new_max_width(19).balanced(true)
	);
	print_result(wrapped_text, 19);
	
	let wrapped_text = wrap_text(
		"VeryLongTextThatShouldWrapAtSomePoint.",
		&Settings::new_max_width(30).balanced(true)
	);
	print_result(wrapped_text, 30);

	let wrapped_text = wrap_text(
		"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed ante eu velit placerat congue. Sed efficitur ultricies odio ac varius. Praesent vulputate molestie enim, id viverra elit venenatis vitae. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Integer et nibh erat. Nulla eget neque eros. Praesent et vehicula lectus. Morbi molestie purus eleifend pharetra egestas. Vivamus vehicula ex tincidunt magna iaculis molestie id tempor massa.",
		&Settings::new_max_width(50).balanced(true)
	);
	print_result(wrapped_text, 50);
}

fn print_result(text: String, width: usize) {
	println!("The result is:");
	let ruler: String = "|".to_string() + &"-".repeat(width-2) + "|";
	println!("{}", ruler);
	println!("{}", text);
	println!("{}", ruler);
}
