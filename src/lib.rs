
//! Terminology:
//!
//! **Logical Line:**  
//!   A logical line is a string of text terminated
//!   by an explicit newline or end of string.
//!
//! **Real Line:**  
//!   A real line is a (modified) substring of a
//!   logical line that can be rendered.
//!   A real line is the product of the wrapper.


use unicode_properties::general_category::GeneralCategory;
use unicode_properties::general_category::GeneralCategoryGroup;
use unicode_segmentation::UnicodeSegmentation;
use unicode_width::UnicodeWidthStr;

mod accumulator;
mod helper;
mod wrap_result;

use accumulator::Accumulator;
use helper::categorize_grapheme;
use helper::format_aware_width;
use helper::general_category_to_general_category_group;
use helper::get_blanks_offset;
use wrap_result::WrapReason;
use wrap_result::WrapResult;

#[derive(Debug,Clone)]
pub struct Settings {
	/// The absolute maximum line width text will be wrapped to.
	///
	/// For values less than 5 most wrapper settings will produce
	/// unpleasant or incorrect results.
	pub max_width: usize,

	/// The minimum line width the wrapper will be aiming for
	/// when deciding on wheter to grapheme wrap or not.
	///
	/// Will loose most of its influence when in balanced mode.
	/// 
	/// Set to 0 to avoid grapheme-wrapping whenever possible. 
	pub early_wrap_width: usize,

	/// If a line is shorter than this width the wrapper
	/// will try to make it longer by taking words from
	/// other lines.
	///
	/// * used to prevent the last and
	///   second to last lines from being too short.
	/// * in balanced mode it also takes the role of the
	///   early_wrap_width to preven too long words from
	///   creating too short lines
	///
	/// This sould be set to a value smaller than the early wrap width.
	///
	/// Note: This won't guarantee a minimum line length.
	///       The wrapper will try its best though.
	///
	/// Set to 0 to disable this behavior.
	pub min_line_width: usize,

	/// Wheter to try and balance line length after wrapping
	/// per paragraph.
	/// 
	/// Enabling this will bypass grapheme wrapping in most cases.
	pub balanced_wrap: bool,
}

impl Settings {

	/// Creates a new Settings struct with the given width
	/// and a sane amount of early wrapping allowed.
	/// It also sets the min_line_width to a sane value.
	pub fn new_max_width(max_width: usize) -> Self {
		Self {
			max_width: max_width,
			early_wrap_width: (max_width as f32*0.62) as usize,
			min_line_width: (max_width as f32*0.3) as usize,
			balanced_wrap: false,
		}
	}

	/// Set a ration between 0 and 1 of the maximum line width after which early
	/// wrapping (to avoid mid-word breaks) is allowed, default is 0.62.
	///
	/// A ratio of 0 will avoid breaking mid-word whenever possible.
	/// A ratio of 1 will fill the line as much as possible before breaking
	/// in any place neccessary (probably mid-word).
	pub fn early_wrap_width_ratio(mut self, ratio: f32) -> Self  {
		self.early_wrap_width = (self.max_width as f32 * ratio) as usize;
		return self;
	}
	
	/// Set a ratio between 0 and 1 of the maximum line width, that the wrapper will
	/// avoid to make shorter than. This is useful to avoid too short las lines.
	/// Default is 0.3.
	///
	/// Set to a value that is smaller than the early wrap ratio.
	///
	/// A ratio of 0 will completely disable detection of too short lines.
	/// A ratio of 1 will classify all lines as too short,
	/// always triggering the rebalancing fallbacks.
	pub fn min_line_width_ratio(mut self, ratio: f32) -> Self  {
		self.early_wrap_width = (self.max_width as f32 * ratio) as usize;
		return self;
	}

	/// When set to true enables balanced mode,
	/// In this mode the wrapper tries to produce lines of
	/// similar length.
	pub fn balanced(mut self, balanced: bool) -> Self {
		self.balanced_wrap = balanced;
		return self;
	}
}

/// Wraps text at grapheme boundaries
///
/// **Note:** there is no space handling in here, because
/// this is already handled by thhe wors wrapping logic.
fn grapheme_wrap_acc(
	text: &str,
	settings: &Settings,
	mut acc: Accumulator,
) -> WrapResult {
	
	#[cfg(test)]
	println!("grapheme wrapping: '{text}'");
	
	let grapheme_iterator = UnicodeSegmentation::graphemes(text, true);
	let leftover_width = format_aware_width(text);

	let mut previous_grapheme_category: GeneralCategory = GeneralCategory::Unassigned;
	let mut old_pre_wrap_opportunity: Option<WrapResult> = None;
	let mut old_pre_wrap_opportunity_width: usize = 0;
	let mut pre_wrap_opportunity: Option<WrapResult> = None;
	let mut pre_wrap_opportunity_width: usize = 0;
	let mut perfect_balance_wrap_opportunity: Option<WrapResult> = None;

	for grapheme in grapheme_iterator {
		let mut current_width = grapheme.width();
		let category = categorize_grapheme(grapheme);

		// Treat a switch to uppercase lettering as a preferred wrap opportunity
		match category {
			GeneralCategory::UppercaseLetter | GeneralCategory::TitlecaseLetter => {
				if acc.offset > 0 &&
					previous_grapheme_category != GeneralCategory::UppercaseLetter
				{
					pre_wrap_opportunity = Some(
						WrapResult::wrap_at(acc.offset, WrapReason::GraphemeUppercase)
							.with_hyphen(true)
					);
					pre_wrap_opportunity_width = acc.width;
				}
			},
			_ => {},
		}

		println!("grapheme: '{grapheme}'");

		// Treat soft hypehns as zero width characters with beak opportunity
		if grapheme == "\u{00AD}" {
			//println!("Softhyphen!");
			pre_wrap_opportunity = Some(
				WrapResult::wrap_at(acc.offset, WrapReason::GraphemeSoftHyphen)
					.with_hyphen(true)
			);
			pre_wrap_opportunity_width = acc.width;
			current_width = 0;
		}

		// Early wrap logic
		if settings.max_width <= current_width + acc.width {
			if pre_wrap_opportunity_width >= settings.early_wrap_width {
				if let Some(pre_wrap_opportunity) = pre_wrap_opportunity {
					return pre_wrap_opportunity;
				}
			}
			// If we have a perfect length break opportunity for
			// balanced wrapping use that.
			if let Some(wrap_opportunity) = perfect_balance_wrap_opportunity {
				return wrap_opportunity;
			}
			// Hard cut at the end and done.
			return WrapResult::wrap_at(acc.offset, WrapReason::GraphemeHardWrap)
				.with_hyphen(true);
		}

		// balanced wrapping logic
		if settings.balanced_wrap {
			if acc.aim_for_line_length <= pre_wrap_opportunity_width {
				// If we have an old_pre_wrap_opportunity that is before the
				// length we are aiming for: evaluate it.
				if old_pre_wrap_opportunity_width < acc.aim_for_line_length
					&& old_pre_wrap_opportunity_width >= settings.min_line_width
				{
					
					let d = pre_wrap_opportunity_width - acc.aim_for_line_length;
					let d_o = acc.aim_for_line_length - old_pre_wrap_opportunity_width;
					if d_o < d {
						if let Some(old_pre_wrap_opportunity) = old_pre_wrap_opportunity {
							return old_pre_wrap_opportunity;
						}
					}
				}
				// If the pre-wrap is over the aim length and the pre-aim (old)
				// pre wrap is further away use the latest pre wrap opportunity.
				if let Some(pre_wrap_opportunity) = pre_wrap_opportunity {
					return pre_wrap_opportunity;
				}
			}
			if perfect_balance_wrap_opportunity.is_none()
				&& acc.width + current_width >= acc.aim_for_line_length
			{
				println!("Perfect Wrap here!");
				perfect_balance_wrap_opportunity = Some(WrapResult::wrap_at(
					acc.offset+grapheme.len(),
					WrapReason::GraphemePerfectBalance
				).with_hyphen(true));
			}
		}

		// Very early wrap in the second to last line
		// to keep the last line from being too short.
		if leftover_width < settings.max_width * 2 {
			if acc.width > settings.min_line_width {
				if leftover_width < settings.min_line_width +acc.width + current_width {
					//println!("Trying a very early wrap to prerve min line width.");
					if pre_wrap_opportunity_width > leftover_width/2 {
						if let Some(pre_wrap_opportunity) = pre_wrap_opportunity {
							return pre_wrap_opportunity;
						}
					}
				}
			}
		}

		if old_pre_wrap_opportunity_width != pre_wrap_opportunity_width {
			old_pre_wrap_opportunity = pre_wrap_opportunity.clone();
			old_pre_wrap_opportunity_width = pre_wrap_opportunity_width;
		}
		previous_grapheme_category = category;
		acc.width  += current_width;
		acc.offset += grapheme.len();
	}
	return WrapResult::wrap_at(acc.offset, WrapReason::GraphemeEnd);
}

/// Warps text at word boundaries
fn word_wrap_acc(
	text: &str,
	settings: &Settings,
	mut acc: Accumulator,
) -> WrapResult {

	#[cfg(test)]
	println!("word wrapping: '{text}'");
	
	let word_iterator = UnicodeSegmentation::split_word_bounds(text);
	let leftover_width = format_aware_width(text);
	let mut wrap_opportunity: Option<WrapResult> = None;

	// short circuit if the line fits.
	if leftover_width <= settings.max_width {
		return WrapResult::wrap_at(text.len(), WrapReason::WordShortcut);
	}

	for word in word_iterator {
		let current_width = format_aware_width(word);
		let category_group = general_category_to_general_category_group(
			categorize_grapheme(word)
		);

		if wrap_opportunity.is_some() {
			if current_width + acc.width <= settings.max_width  {
				if category_group == GeneralCategoryGroup::Punctuation {
					return WrapResult::wrap_at(
						acc.offset+word.len(),
						WrapReason::WordTakeWithPoint
					);
				}
			}
			if let Some(wrap_opportunity) = wrap_opportunity {
				return wrap_opportunity;
			}
		}
		
		println!("word: '{word}' [{}-{}]", acc.width, acc.width+current_width);

		// Second to last line handler
		if leftover_width < settings.max_width*2 {
			// If the last line would be too short use the grapheme wrapper
			// (it is able to handle balancing those to get a not too short
			// last line and a slightly longer second to last line)
			//
			// This should kick in if:
			// 1. We are under the min_line_width ourselfes.
			// 2. The next word would starve the last line.
			println!("Last line");
			if settings.min_line_width + acc.width + current_width > leftover_width {
				println!("Trying to avoid starving last line.");
				// If we can't satisfy both lines by word wrapping
				// try grapheme wrapping.
				if acc.width < settings.min_line_width {
					return grapheme_wrap_acc(&text[acc.offset..], settings, acc);
				} else {
					return WrapResult::wrap_at(
						acc.offset,
						WrapReason::WordVeryEarly
					);
				}
			}
		}
		
		// Early and regular wrapping logic
		if settings.max_width < current_width + acc.width {
			// Fall back to grapheme wrapping if the words results
			// in too much of a space
			if (acc.width <= settings.early_wrap_width && !settings.balanced_wrap)
				|| acc.width < settings.min_line_width
			{
				return grapheme_wrap_acc(&text[acc.offset..], settings, acc.clone());
			}
			
			// Do a regular "early" wrap.
			wrap_opportunity = Some(
				WrapResult::wrap_at(acc.offset, WrapReason::WordEarly)
			);
		}

		// balanced wrapping logic
		if settings.balanced_wrap && wrap_opportunity.is_none() {
			// Leave if we are over our aim.
			if acc.aim_for_line_length <= acc.width {
				wrap_opportunity = Some(
					WrapResult::wrap_at(acc.offset, WrapReason::WordBalanceOver)
				);
			// Take if we are on or over but only if we are not processing a space.
			} else if acc.aim_for_line_length <= acc.width + current_width {
				let d_take = (acc.width + current_width) - acc.aim_for_line_length;
				let d_leave = acc.aim_for_line_length - acc.width;
				// Put leaving at a slight disadvantage here (when both scores are
				// equal it looses) to make up for the unaccounted space before the
				// token.
				if d_leave < d_take {
					wrap_opportunity = Some(
						WrapResult::wrap_at(acc.offset, WrapReason::WordBalanceLeave)
					);
				} else {
					// if the word is crossing the boundary we are aiming for.
					wrap_opportunity = Some(
						WrapResult::wrap_at(acc.offset + word.len(), WrapReason::WordBalanceTake)
					);
				}
			}
		}
		acc.width += current_width;
		acc.offset += word.len();
	}
	return WrapResult::wrap_at(acc.offset, WrapReason::WordEnd);
}

/// Generic configurable word wrapping function with all features.
///
/// If you are unsure which one to use, use this one.
pub fn wrap_text(
	text: &str,
	settings: &Settings,
) -> String {

	#[cfg(test)]
	println!("Wrap Settings: {:?}", settings);
	
	let mut out: String = "".to_string();
	let text_len = text.len();
	let mut last_end_index = 0;
	let mut acc = Accumulator::new();

	// Set up balanced wrappinng
	if settings.balanced_wrap {
		let total_width = format_aware_width(text);
		println!("Text width: {total_width}");
		// Estimate how many lines will be needed
		let lines = (((total_width/settings.max_width)+1)as f32 * 1.1)as usize;
		println!("Lines predicted: {lines}");
		// Estimate the line-length we should be aiming for
		// to get an even distribution of text.
		acc.aim_for_line_length = total_width/lines;
		println!("aim for length: {}", acc.aim_for_line_length);
	}
	
	while text_len > last_end_index {
		// discard some blanks
		let blanks_offset = get_blanks_offset(&text[last_end_index..]);
		last_end_index += blanks_offset;
		// Word wrap the whole thing
		let mut r = word_wrap_acc(&text[last_end_index..], settings, acc);
		acc.next_real_line();
		println!("Wrap result: {:?}", r);
		if r.index_end == 0 {
			println!("Word wrapping failed! Trying to grapheme-wrap directly.");
			//TODO: panic here by default when running tests.
			acc.reset_real_line();
			r = grapheme_wrap_acc(&text[last_end_index..], settings, acc);
			println!("Wrap result: {:?}", r);
		}
		if r.index_end == 0 {
			if cfg!(test) {
				// Loud bang when testing
				panic!("Both word_wrap_acc() and grapheme_wrap_acc() returned an index_end of 0 which would have resulted in an unwanted early termination in production!");
			} else {
				// Graceful failure in prod (this shouldn't crash an app!)
				out += "…";
				break;
			}
		}
		// Apply wrap result
		let wrap_text = &text[last_end_index..last_end_index+r.index_end];
		out += wrap_text.trim();
		if last_end_index + r.index_end >= text_len {
			// Do nothing when at end of text
		} else if r.insert_hyphen {
			//TODO: Choose an appropriate hyphen instead of always using the
			//      one for latin letters.
			out += "-\n";
		} else {
			out += "\n";
		}
		last_end_index += r.index_end;
	}
	// Remove soft hyphens
	if out.contains("\u{00AD}") {
		out = out.replace("\u{00AD}","");
	}
	return out;
}

#[cfg(test)]
mod tests {
    use super::*;

	// Most of the line lengths and texts of these test also try to
	// test for off-by-one errors.

	/// Sanity check to make sure that single line stuff that doesn't
	/// need wrapping won't get wrapped.
    #[test]
    fn single_line_of_text() {
		let wrapped_text = wrap_text(
			"Single line of Text. 📝",
			&Settings::new_max_width(23)
		);
		assert_eq!(wrapped_text, "Single line of Text. 📝");
    }

	/// Regular, non-balanced text-wrapping
	#[test]
    fn wrapped_words() {
		let wrapped_text = wrap_text(
			"The quick brown fox🦊 jumps over the lazy dog🐕.",
			&Settings::new_max_width(19)
		);
        assert_eq!(wrapped_text,
"The quick brown fox
🦊 jumps over the
lazy dog🐕."
        );

    }

    /// Exercises the balance wrapping logic in the word wrapper.
    #[test]
    fn balance_wrapped_words() {
		let wrapped_text = wrap_text(
			"The quick brown fox🦊 jumps over the lazy dog🐕.",
			&Settings::new_max_width(20).balanced(true)
		);
        assert_eq!(wrapped_text,
"The quick brown
fox🦊 jumps over
the lazy dog🐕."
        );
    }

	/// Exercises the balance wrapping logic in the grapheme wrapper
	#[test]
    fn balance_wrapped_soft_hyphens() {
		let wrapped_text = wrap_text(
			"Very­long­text­that­should­wrap­at­some­point.",
			&Settings::new_max_width(30).balanced(true)
		);
        assert_eq!(wrapped_text,
"Verylongtextthatshould-
wrapatsomepoint."
        );
    }

	/// Exercises the balance wrapping logic in the grapheme wrapper
	#[test]
    fn balance_wrapped_camel_case() {
		let wrapped_text = wrap_text(
			"VeryLongTextThatShouldWrapAtSomePoint.",
			&Settings::new_max_width(30).balanced(true)
		);
        assert_eq!(wrapped_text,
"VeryLongTextThatShould-
WrapAtSomePoint."
        );
    }

	/// Sanity check to make sure that wrapping
	/// very long texts without a break opportunity works.
    #[test]
    fn balance_wrapped_lower_case() {
		let wrapped_text = wrap_text(
			"Verylongtextthatshouldwrapatsomepoint.",
			&Settings::new_max_width(30).balanced(true)
		);
        assert_eq!(wrapped_text,
"Verylongtextthatsho-
uldwrapatsomepoint."
        );
    }

	/// Make sure the word wrapper doesn't fall over when
	/// balance wrapping long "words" with short words around.
    #[test]
    fn balance_wrapped_short_around_long_word() {
		let wrapped_text = wrap_text(
			"VERY LONGTEXTTHATSHOULDWRAPATSOME POINT.",
			&Settings::new_max_width(30).balanced(true)
		);
        assert_eq!(wrapped_text,
"VERY LONGTEXTTHATSHO-
ULDWRAPATSOME POINT."
        );
    }

	/// Make sure the word wrapper doesn't fall over when
	/// long "words" with short words around gget non-balance wrapped
    #[test]
    fn short_around_long_word() {
		let wrapped_text = wrap_text(
			"VERY LONGTEXTTHATSHOULDWRAPATSOME POINT.",
			&Settings::new_max_width(30)
		);
        assert_eq!(wrapped_text,
"VERY LONGTEXTTHATSHOULDWRAPAT-
SOME POINT."
        );
    }

    
	/// Make sure the word wrapper doesn't fall over when
	/// balance wrapping long "words"
    #[test]
    fn balance_wrapped_long_word() {
		let wrapped_text = wrap_text(
			"VERY LONGTEXTTHAT SHOULDWRAPATSOME POINT.",
			&Settings::new_max_width(30).balanced(true)
		);
        assert_eq!(wrapped_text,
"VERY LONGTEXTTHAT
SHOULDWRAPATSOME POINT."
        );
    }

    /// Exercises the last-line balancing part in the grapheme wrapper.
	#[test]
    fn last_line_soft_hyphens() {
		let wrapped_text = wrap_text(
			"Very­long­text­that­should­wrap­at­some­point.",
			&Settings::new_max_width(11).balanced(true)
		);
        assert_eq!(wrapped_text,
"Verylong-
textthat-
shouldwrap-
atsome-
point."
        );
    }

    /// Exercises the last-line balancing part in the grapheme wrapper.
	#[test]
    fn last_line_camel_case() {
		let wrapped_text = wrap_text(
			"VeryLongTextThatShouldWrapAtSomePoint.",
			&Settings::new_max_width(11)
		);
        assert_eq!(wrapped_text,
"VeryLong-
TextThat-
ShouldWrap-
AtSome-
Point."
        );
    }


}
