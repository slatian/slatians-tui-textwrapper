use unicode_properties::general_category::GeneralCategory;
use unicode_properties::general_category::GeneralCategoryGroup;
use unicode_properties::general_category::UnicodeGeneralCategory;
use unicode_width::UnicodeWidthStr;

/// Categorizes the first character of a string to get an idea of
/// what the grapheme the string represents actually is.
pub fn categorize_grapheme(grapheme: &str) -> GeneralCategory {
	for c in grapheme.chars() {
		return c.general_category();
	}
	return GeneralCategory::Unassigned;
}

pub fn general_category_to_general_category_group(
	category: GeneralCategory
) -> GeneralCategoryGroup {
	match category {
		GeneralCategory::UppercaseLetter |
		GeneralCategory::LowercaseLetter |
		GeneralCategory::TitlecaseLetter |
		GeneralCategory::ModifierLetter |
		GeneralCategory::OtherLetter =>
			GeneralCategoryGroup::Letter,
		GeneralCategory::NonspacingMark |
		GeneralCategory::SpacingMark |
		GeneralCategory::EnclosingMark =>
			GeneralCategoryGroup::Mark,
		GeneralCategory::DecimalNumber |
		GeneralCategory::LetterNumber |
		GeneralCategory::OtherNumber =>
			GeneralCategoryGroup::Number,
		GeneralCategory::ConnectorPunctuation |
		GeneralCategory::DashPunctuation |
		GeneralCategory::OpenPunctuation |
		GeneralCategory::ClosePunctuation |
		GeneralCategory::InitialPunctuation |
		GeneralCategory::FinalPunctuation |
		GeneralCategory::OtherPunctuation =>
			GeneralCategoryGroup::Punctuation,
		GeneralCategory::MathSymbol |
		GeneralCategory::CurrencySymbol |
		GeneralCategory::ModifierSymbol |
		GeneralCategory::OtherSymbol =>
			GeneralCategoryGroup::Symbol,
		GeneralCategory::SpaceSeparator |
		GeneralCategory::LineSeparator |
		GeneralCategory::ParagraphSeparator =>
			GeneralCategoryGroup::Separator,
		GeneralCategory::Control |
		GeneralCategory::Format |
		GeneralCategory::Surrogate |
		GeneralCategory::PrivateUse |
		GeneralCategory::Unassigned =>
			GeneralCategoryGroup::Other
	}
}

/// Tells at wich offset the blank/space characters at the start of the given text stop.
pub fn get_blanks_offset(text: &str) -> usize {
	
	for (offset,c) in text.char_indices() {
		if c.general_category() != GeneralCategory::SpaceSeparator {
			return offset;
		}
	}
	return text.len();
}

/// Tells the width of some teext, but corrects for soft hyphens
///
/// This will be expanded to also handle tabs.
pub fn format_aware_width(text: &str) -> usize {
	let mut width = text.width();
	// We can get away with iterating characters for now
	for c in text.chars() {
		// Soft hyphens have a width of 1 as per unicode,
		// but they are removed for wrapping.
		if c == '\u{00AD}' {
			width -= 1;
		}
	}
	return width;
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_format_aware_width() {
		let width = format_aware_width("Foo bar baz! 😃👪");
		assert_eq!(width, 17);
	}

	#[test]
	fn test_format_aware_width_with_soft_hyphen() {
		let width = format_aware_width("Foo\u{00AD}bar\u{00AD}baz! 😃👪");
		assert_eq!(width, 15);
	}
}
