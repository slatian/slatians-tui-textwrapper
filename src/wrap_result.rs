/// The reason why the text was wrapped.
/// Helps with debugging.
///
/// For every code path that generates a word wrap
/// there is an entry in here. **Reusing is not allowed!**
///
/// In order of appearance.
///
/// Thanks to Bisqwit for this trick!
/// (Though it was intended for a compiler.)
#[derive(Debug,Clone,PartialEq,Eq)]
pub enum WrapReason {
	// grapheme wrap
	GraphemeUppercase,
	GraphemeSoftHyphen,
	GraphemePerfectBalance,
	GraphemeHardWrap,
	GraphemeEnd,

	// word wrap
	WordShortcut,
	WordTakeWithPoint,
	WordVeryEarly,
	WordEarly,
	WordBalanceOver,
	WordBalanceLeave,
	WordBalanceTake,
	WordEnd,
	
}

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct WrapResult {
	/// where to apply the break
	pub index_end: usize,

	/// wheter to insert a hyphen after the text ends
	pub insert_hyphen: bool,

	/// Why the wrap occourred.
	/// (It is okay to never read this in production)
	#[allow(dead_code)]
	pub reason: WrapReason,
}

impl WrapResult {

	pub fn wrap_at(index: usize, reason: WrapReason) -> Self {
		Self {
			index_end: index,
			insert_hyphen: false,
			reason: reason,
		}
	}

	pub fn with_hyphen(mut self, insert_hyphen: bool) -> Self {
		self.insert_hyphen = insert_hyphen;
		return self;
	}
}

